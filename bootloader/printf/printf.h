// Lightweight console print functions
int dbprintf(const char *format, ...);
int dbsprintf(char *out, const char *format, ...);
