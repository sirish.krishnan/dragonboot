/*
                 ___====-_  _-====___
           _--^^^#####//      \\#####^^^--_
        _-^##########// (    ) \\##########^-_
       -############//  |\^^/|  \\############-
     _/############//   (@::@)   \\############\_
    /#############((     \\//     ))#############\
   -###############\\    (oo)    //###############-
  -#################\\  / VV \  //#################-
 -###################\\/      \//###################-
_#/|##########/\######(   /\   )######/\##########|\#_
|/ |#/\#/\#/\/  \#/\##\  |  |  /##/\#/  \/\#/\#/\#| \|
`  |/  V  V  `   V  \#\| |  | |/#/  V   '  V  V  \|  '
   `   `  `      `   / | |  | | \   '      '  '   '
                    (  | |  | |  )
                   __\ | |  | | /__
                  (vvv(VVV)(VVV)vvv)

                       DragonBoot

******************************************************************************/

/*!
 * \file main.c
 * \brief Entry-point to the Dragonboot bootloader state-machine
 *
 * This file houses the implementation of the entry-point 
 * to the DragonBoot interactive bootloader.
 */

/*****************************************************************************/

/* Include files */
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "microrl/microrl.h"
#include "inc/tm4c123gh6pm.h"
#include "microrl/config.h"
#include "platform/platform.h"
#include "console/console.h"
#include "printf/printf.h"

/* Macros */
#define TEST_BEARSSL 0

/* Function prototypes */
extern int test_bearssl();

/* Create microrl object and pointer on it */
microrl_t rl;
microrl_t * prl = &rl;

//*****************************************************************************

/*!
 * \brief This API kick-starts the DragonBoot interactive boot console
 * 
 * This function does the following :
 *  - Invoke platform initialization routine
 *  - Invoke Microrl CLI initialization
 *  - Invoke a routine to start processing user commands
 */

int start_dragonboot (void/*int argc, char ** argv*/)
{
    init ();
    // call init with ptr to microrl instance and print callback
    DEBUG("Calling microrl_init()\r");
    microrl_init (prl, print);
    // set callback for execute
    DEBUG("Calling microrl_set_execute_callback()\r");
    microrl_set_execute_callback (prl, execute);

#ifdef _USE_COMPLETE
    // set callback for completion
    DEBUG("Calling microrl_set_complete_callback()\r");
    microrl_set_complete_callback (prl, complet);
#endif
    // set callback for Ctrl+C
    DEBUG("Calling microrl_set_sigint_callback()\r");
    microrl_set_sigint_callback (prl, sigint);

    while (1) {
        // put received char from stdin to microrl lib
        microrl_insert_char (prl, get_char());
    }
    return 0;
}

/*****************************************************************************/

/*!
 * \brief Main (entry-point)
 * 
 * This function starts the DragonBoot interactive boot console.
 */
int main()
{
#if (TEST_BEARSSL==1)
    init ();
    test_bearssl();
#else
    start_dragonboot();
#endif
    dbprintf("\rIdling...\r");
    while(1)
    {
        //dbprintf("Idling...\n");
    }
    return 0;
}

/*****************************************************************************/
